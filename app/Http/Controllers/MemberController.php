<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = \App\Member::where('user_id', auth('api')->user()->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $members
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'full_name' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'member_category_id' => 'required',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $member = new \App\Member;
            $member->user_id = auth('api')->user()->id;
            $member->member_category_id = $request->get('member_category_id');
            $member->full_name = $request->get('full_name');
            $member->dob = $request->get('dob');
            $member->address = $request->get('address');
            $member->gender = $request->get('gender');

            $random = Str::random(3);
            $barcode = $request->get('dob');
            $barcode = str_replace('-', '', $barcode);
            $member->barcode = $barcode . $random;
            $member->save();

            if ($member) {
                $status = "success";
                $message = "add member successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'add member failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'full_name' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
            'member_category_id' => 'required',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $member = \App\Member::findOrFail($id);
            $member->user_id = auth('api')->user()->id;
            $member->member_category_id = $request->get('member_category_id');
            $member->full_name = $request->get('full_name');
            $member->dob = $request->get('dob');
            $member->address = $request->get('address');
            $member->gender = $request->get('gender');
            $member->save();

            if ($member) {
                $status = "success";
                $message = "update member successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update member failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Member::where('user_id', auth('api')->user()->id)->findOrFail($id)->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'delete member successfully',
            'data' => null
        ], 200);
    }
}
