<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_categories = \App\ProductCategory::where('user_id', auth('api')->user()->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $product_categories
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $product_category = new \App\ProductCategory;
            $product_category->user_id = auth('api')->user()->id;
            $product_category->name = $request->get('name');
            $product_category->save();

            if ($product_category) {
                $status = "success";
                $message = "add product category successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'add product category failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $product_category = \App\ProductCategory::findOrFail($id);
            $product_category->user_id = auth('api')->user()->id;
            $product_category->name = $request->get('name');
            $product_category->save();

            if ($product_category) {
                $status = "success";
                $message = "update product category successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update product category failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\ProductCategory::where('user_id', auth('api')->user()->id)->findOrFail($id)->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'delete product category successfully',
            'data' => null
        ], 200);
    }
}
