<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = \App\Product::where('user_id', auth('api')->user()->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $products
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'desc' => 'required|string',
            'price' => 'required|integer',
            'stock' => 'required|integer',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $product = new \App\Product;
            $product->product_category_id = $request->get('product_category_id');
            $product->user_id = auth('api')->user()->id;
            $product->name = $request->get('name');
            $product->desc = $request->get('desc');
            $product->price = $request->get('price');
            $product->stock = $request->get('stock');

            if ($request->file('image')) {
                $product->image = $request->file('image')->store('products', 'public');
            }

            $product->save();

            if ($product) {
                $status = "success";
                $message = "add product successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'add product failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'desc' => 'required|string',
            'price' => 'required|integer',
            'stock' => 'required|integer',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $product = \App\Product::findOrFail($id);
            $product->product_category_id = $request->get('product_category_id');
            $product->user_id = auth('api')->user()->id;
            $product->name = $request->get('name');
            $product->desc = $request->get('desc');
            $product->price = $request->get('price');
            $product->stock = $request->get('stock');

            if ($request->file('image')) {
                if ($product->image && file_exists(storage_path('app/public/' . $product->image))) {
                    \Storage::delete('public/' . $product->image);
                }

                $product->image = $request->file('image')->store('products', 'public');
            }
            
            $product->save();

            if ($product) {
                $status = "success";
                $message = "update product successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update product failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = \App\Product::where('user_id', auth('api')->user()->id)->findOrFail($id);

        if ($product->image && file_exists(storage_path('app/public/' . $product->image))) {
            \Storage::delete('public/' . $product->image);
        }
        
        $product->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'delete product successfully',
            'data' => null
        ], 200);
    }
}
