<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = \App\Transaction::with('product')->where('user_id', auth('api')->user()->id)->orderBy('created_at', 'desc')->get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $transactions
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction_number = date("ymdHis");
        foreach ($request->transaction as $value) {
            $transaction = new \App\Transaction();
            $transaction->user_id = $request->user_id;
            $transaction->product_id = $value["product_id"];
            $transaction->member_id = $request->member_id;
            $transaction->trx_number = $transaction_number;
            $transaction->quantity = $value["quantity"];
            $transaction->discount_id = $request->discount_id;
            $transaction->total = $value["total"];
            $transaction->save();

            $product = \App\Product::findOrFail($value["product_id"]);
            $product->stock -= $value["quantity"];
            $product->save();

        }

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => null
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
