<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255|min:5',
            'email' => 'required|string|email|max:255|unique:users',
            'business_name' => 'required|string|min:5|max:255',
            'password' => 'required|string|min:6',
        ]);
  
        $status = "error";
        $message = "";
        $data = null;
        $code = 400;
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $user = new \App\User;
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->business_name = $request->get('business_name');
            $user->password = \Hash::make($request->get('password'));
            $user->save();

            if($user) {
                $user->generateToken();

                $status = "success";
                $message = "register successfully";
                $data = $user;
                $code = 200;
            } else {
                $message = 'register failed';
            }
        }
  
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
  
        $status = "error";
        $message = "";
        $data = null;
        $code = 302;

        $user = \App\User::where('email', '=', $request->get('email'))->first();
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else if ($user) {
            if(\Hash::check($request->get('password'), $user->password)) {
                $user->generateToken();

                $status = 'success';
                $message = 'Login sukses';
                $data = $user;
                $code = 200;
            } else {
                $message = "Login gagal, password salah";
            }
        } else {
            $message = "Login gagal, user " . $request->email . " tidak ditemukan";
        }
  
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function logout(Request $request)
    {
        $user = auth('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'logout berhasil',
            'data' => null
        ], 200);
    }
}
