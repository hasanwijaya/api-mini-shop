<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');
Route::resource('member-categories', 'MemberCategoryController');

Route::middleware('auth:api')->group(function() {
    Route::post('/logout', 'UserController@logout');
    
    Route::resource('product-categories', 'ProductCategoryController');
    Route::resource('products', 'ProductController');
    Route::resource('transactions', 'TransactionController');
    Route::resource('members', 'MemberController');
    Route::resource('discounts', 'DiscountController');
});
